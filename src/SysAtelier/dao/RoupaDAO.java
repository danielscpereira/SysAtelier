package SysAtelier.dao;

import SysAtelier.model.Roupa;
import java.util.List;
import javax.persistence.TypedQuery;

public class RoupaDAO extends DAO<Roupa>{

    public RoupaDAO() {
        super(Roupa.class);
    }
    
    @Override
    public void update(Roupa roupa) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(roupa);
        em.getTransaction().commit();
        em.close();
    }
    
    @Override
    public void save(Roupa roupa) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(roupa);
        em.getTransaction().commit();
        em.close();
    }
    
    public List<Roupa> findBYTipoGenero(String tipo, String genero) {
        
        this.em = JPAUtil.getEntityManager();
        String hql = ("SELECT r FROM Roupa r WHERE 1=1 ");

        if (!tipo.equals("")) {
            hql += " AND r.tipoFantasia = :tipo";
        }
        
        if (!genero.equals("")) {
            hql += " AND r.generoFantasia = :genero";
        }
     
        TypedQuery<Roupa> query = em.createQuery(hql, Roupa.class);

        if (!tipo.equals("")) {
           query.setParameter("tipo", tipo);
        }
        
        if (!genero.equals("")) {
            query.setParameter("genero", genero);
        }
        
        List<Roupa> lista = query.getResultList();

        return lista;
    }
    
    public List<Roupa> findBYDescricao (String descricao){
        
        this.em = JPAUtil.getEntityManager();
        String hql = ("SELECT r FROM Roupa r WHERE 1=1 ");
        
        if (!descricao.equals("")) {
            hql += " AND r.descricao = :descricao";
        }
        
        TypedQuery<Roupa> query = em.createQuery(hql, Roupa.class);
        
        if (!descricao.equals("")) {
            query.setParameter("descricao", descricao);
        }
        
        List<Roupa> lista = query.getResultList();

        return lista;
    }
    
}
