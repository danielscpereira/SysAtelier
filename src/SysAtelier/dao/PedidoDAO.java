package SysAtelier.dao;

import SysAtelier.model.Pedido;

public class PedidoDAO extends DAO<Pedido>{

    public PedidoDAO() {
        super(Pedido.class);
    }
    
   @Override
    public void update(Pedido pedido) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(pedido);
    //    em.merge(pedido.getEncomendaRoupa());
     //   em.merge(pedido.getRoupa());
        em.getTransaction().commit();
        em.close();
    }
    
    @Override
    public void save(Pedido pedido) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(pedido);
 //       em.persist(pedido.getEncomendaRoupa());
 //      em.persist(pedido.getRoupa());
        em.getTransaction().commit();
        em.close();
    }
     
}

