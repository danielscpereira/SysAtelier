package SysAtelier.dao;

import SysAtelier.model.Cliente;
import java.util.List;
import javax.persistence.TypedQuery;

public class ClienteDAO extends DAO<Cliente> {

    public ClienteDAO() {
        super(Cliente.class);
    }

    @Override
    public void update(Cliente cliente) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(cliente.getEndereco());
        em.merge(cliente);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void save(Cliente cliente) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(cliente.getEndereco());
        em.persist(cliente);
        em.getTransaction().commit();
        em.close();
    }

    public List<Cliente> findBYUnit(int id, String CPF, String nome) {

        this.em = JPAUtil.getEntityManager();
        String hql = ("SELECT c FROM Cliente c WHERE 1=1 ");

        if (id != 0) {
            hql += " AND c.id = :id";
        }

        if (!CPF.equals("")) {
            hql += " AND c.CPF = :cpf";
        }

        if (!nome.equals("")) {
            hql += " AND c.nome like :nome";
        }

        TypedQuery<Cliente> query = em.createQuery(hql, Cliente.class);

        if (id != 0) {
            query.setParameter("id", id);
        }

        if (!CPF.equals("")) {
            query.setParameter("cpf", CPF);
        }

        if (!nome.equals("")) {
            query.setParameter("nome", "%" +  nome + "%");
        }

        List<Cliente> lista = query.getResultList();

        return lista;
    }

    public List<Cliente> findByAll() {

        return em.createQuery("SELECT * FROM Cliente c WHERE 1=1").getResultList();

    }
    
    

}
