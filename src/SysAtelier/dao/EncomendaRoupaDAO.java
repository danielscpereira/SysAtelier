/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SysAtelier.dao;

import SysAtelier.model.EncomendaRoupa;

/**
 *
 * @author sala303b
 */
public class EncomendaRoupaDAO extends DAO<EncomendaRoupa>{

    public EncomendaRoupaDAO() {
        super(EncomendaRoupa.class);
    }
    
    @Override
    public void update(EncomendaRoupa encomendaRoupa) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(encomendaRoupa);
        em.getTransaction().commit();
        em.close();
    }
    
    @Override
    public void save(EncomendaRoupa encomendaRoupa) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(encomendaRoupa);
        em.getTransaction().commit();
        em.close();
    }
    
}
