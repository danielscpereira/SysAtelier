package SysAtelier.model;

import javax.persistence.Entity;

@Entity
public class EncomendaRoupa extends Entidade {

    private int busto;
    private int cintura;
    private int quadril;
    private int pescoco;
    private int torax;
    private int braco;
    private int punho;
    private int alturaCostas;
    private int larguraCostas;
    private int alturaBusto;
    private int larguraBusto;
    private int comprimentoManga;
    private int alturaQuadril;
    private int comprimentoSaia;
    private int comprimentoCalca;
    private int alturaEntrePernas;
    private int alturaGancho;
    private int larguraJoelho;
    private int bocaCalca;
    private String generoFantasia;
    private String tipoFantasia;

    public EncomendaRoupa() {
    }

    public int getBusto() {
        return busto;
    }

    public void setBusto(int busto) {
        this.busto = busto;
    }

    public int getCintura() {
        return cintura;
    }

    public void setCintura(int cintura) {
        this.cintura = cintura;
    }

    public int getQuadril() {
        return quadril;
    }

    public void setQuadril(int quadril) {
        this.quadril = quadril;
    }

    public int getPescoco() {
        return pescoco;
    }

    public void setPescoco(int pescoco) {
        this.pescoco = pescoco;
    }

    public int getTorax() {
        return torax;
    }

    public void setTorax(int torax) {
        this.torax = torax;
    }

    public int getBraco() {
        return braco;
    }

    public void setBraco(int braco) {
        this.braco = braco;
    }

    public int getPunho() {
        return punho;
    }

    public void setPunho(int punho) {
        this.punho = punho;
    }

    public int getAlturaCostas() {
        return alturaCostas;
    }

    public void setAlturaCostas(int alturaCostas) {
        this.alturaCostas = alturaCostas;
    }

    public int getLarguraCostas() {
        return larguraCostas;
    }

    public void setLarguraCostas(int larguraCostas) {
        this.larguraCostas = larguraCostas;
    }

    public int getAlturaBusto() {
        return alturaBusto;
    }

    public void setAlturaBusto(int alturaBusto) {
        this.alturaBusto = alturaBusto;
    }

    public int getLarguraBusto() {
        return larguraBusto;
    }

    public void setLarguraBusto(int larguraBusto) {
        this.larguraBusto = larguraBusto;
    }

    public int getComprimentoManga() {
        return comprimentoManga;
    }

    public void setComprimentoManga(int comprimentoManga) {
        this.comprimentoManga = comprimentoManga;
    }

    public int getAlturaQuadril() {
        return alturaQuadril;
    }

    public void setAlturaQuadril(int alturaQuadril) {
        this.alturaQuadril = alturaQuadril;
    }

    public int getComprimentoSaia() {
        return comprimentoSaia;
    }

    public void setComprimentoSaia(int comprimentoSaia) {
        this.comprimentoSaia = comprimentoSaia;
    }

    public int getComprimentoCalca() {
        return comprimentoCalca;
    }

    public void setComprimentoCalca(int comprimentoCalca) {
        this.comprimentoCalca = comprimentoCalca;
    }

    public int getAlturaEntrePernas() {
        return alturaEntrePernas;
    }

    public void setAlturaEntrePernas(int alturaEntrePernas) {
        this.alturaEntrePernas = alturaEntrePernas;
    }

    public int getAlturaGancho() {
        return alturaGancho;
    }

    public void setAlturaGancho(int alturaGancho) {
        this.alturaGancho = alturaGancho;
    }

    public int getLarguraJoelho() {
        return larguraJoelho;
    }

    public void setLarguraJoelho(int larguraJoelho) {
        this.larguraJoelho = larguraJoelho;
    }

    public int getBocaCalca() {
        return bocaCalca;
    }

    public void setBocaCalca(int bocaCalca) {
        this.bocaCalca = bocaCalca;
    }

    public String getGeneroFantasia() {
        return generoFantasia;
    }

    public void setGeneroFantasia(String generoFantasia) {
        this.generoFantasia = generoFantasia;
    }

    public String getTipoFantasia() {
        return tipoFantasia;
    }

    public void setTipoFantasia(String tipoFantasia) {
        this.tipoFantasia = tipoFantasia;
    }

}
