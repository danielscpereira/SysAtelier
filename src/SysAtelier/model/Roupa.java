package SysAtelier.model;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
public class Roupa extends Entidade{
    
    private boolean alugado = false;
    private double precoRoupa;
    private String generoFantasia;
    private String tipoFantasia;
    private int quantidade;
    private String tamanho;
    private String descricao;
    @Lob
    private byte[] image;
    
    public Roupa() {
    }

    public boolean isAlugado() {
        return alugado;
    }

    public void setAlugado(boolean alugado) {
        this.alugado = alugado;
    }

    public double getPrecoRoupa() {
        return precoRoupa;
    }

    public void setPrecoRoupa(double precoRoupa) {
        this.precoRoupa = precoRoupa;
    }

    public String getGeneroFantasia() {
        return generoFantasia;
    }

    public void setGeneroFantasia(String generoFantasia) {
        this.generoFantasia = generoFantasia;
    }

    public String getTipoFantasia() {
        return tipoFantasia;
    }

    public void setTipoFantasia(String tipoFantasia) {
        this.tipoFantasia = tipoFantasia;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

}
