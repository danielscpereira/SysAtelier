package SysAtelier.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ItemDePedido extends Entidade {

    private int quantidade;
    @ManyToOne(targetEntity = Roupa.class)
    private Roupa roupa;
    private float preco;

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Roupa getRoupa() {
        return roupa;
    }

    public void setRoupa(Roupa roupa) {
        this.roupa = roupa;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

}
