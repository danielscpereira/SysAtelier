package SysAtelier.model;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PedidoSobMedida extends Entidade {

    @Temporal(TemporalType.DATE)
    private Date dataEntrega;
    private float valorAdiantado;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Cliente cliente;
    @Temporal(TemporalType.DATE)
    private Date dataPedido;

    @OneToOne(targetEntity = EncomendaRoupa.class, cascade = CascadeType.PERSIST)
    private EncomendaRoupa emcomenda;

    public PedidoSobMedida() {
        this.dataPedido = new Date();
        this.emcomenda = new EncomendaRoupa();
        this.cliente = new Cliente();
        
    }

    
    
    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public float getValorAdiantado() {
        return valorAdiantado;
    }

    public void setValorAdiantado(float valorAdiantado) {
        this.valorAdiantado = valorAdiantado;
    }

    public EncomendaRoupa getEmcomenda() {
        return emcomenda;
    }

    public void setEmcomenda(EncomendaRoupa emcomenda) {
        this.emcomenda = emcomenda;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }
    

}
