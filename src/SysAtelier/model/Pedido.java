package SysAtelier.model;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class Pedido<T> extends Entidade {

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Cliente cliente;
    @Temporal(TemporalType.DATE)
    private Date dataPedido;
    
    @ManyToMany
    private List<T> itens ;

    public Pedido() {
        this.cliente = new Cliente();
    }
    
    public Pedido(Date dataPedido) {
        this();
        this.dataPedido = dataPedido;
    }

    public Pedido(Cliente cliente, Date dataPedido, List<T> itens) {
        this.cliente = cliente;
        this.dataPedido = dataPedido;
        this.itens = itens;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public List<T> getItens() {
        return itens;
    }

    public void setItens(List<T> itens) {
        this.itens = itens;
    }
    
}
