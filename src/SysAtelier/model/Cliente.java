package SysAtelier.model;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Cliente extends Entidade {

    @Temporal(TemporalType.DATE)
    private Date dataCadastro;
    private String nome;
    private String CPF;
    private String RG;
    private String telFixo;
    private String telCelular;
    private String sexo;
    private String email;
    @OneToOne(cascade = CascadeType.PERSIST)
    private Endereco endereco;

    public Cliente() {
        this.endereco = new Endereco();
        this.dataCadastro = new Date();
    }

    public Cliente(Date dataCadastro) {
        this();
        this.dataCadastro = dataCadastro;
    }

    public Cliente(Date dataCadastro, String nome, String CPF, String RG, String telFixo, String telCelular, String sexo, String email, Endereco endereco) {

        this.dataCadastro = dataCadastro;
        this.nome = nome;
        this.CPF = CPF;
        this.RG = RG;
        this.telFixo = telFixo;
        this.telCelular = telCelular;
        this.sexo = sexo;
        this.email = email;
        this.endereco = endereco;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getRG() {
        return RG;
    }

    public void setRG(String RG) {
        this.RG = RG;
    }

    public String getTelFixo() {
        return telFixo;
    }

    public void setTelFixo(String telFixo) {
        this.telFixo = telFixo;
    }

    public String getTelCelular() {
        return telCelular;
    }

    public void setTelCelular(String telCelular) {
        this.telCelular = telCelular;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

   

}
