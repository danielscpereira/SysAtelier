package SysAtelier.views;

import SysAtelier.dao.ClienteDAO;
import SysAtelier.dao.JPAUtil;
import SysAtelier.dao.PedidoDAO;
import SysAtelier.model.Cliente;
import SysAtelier.model.Pedido;
import SysAtelier.model.Roupa;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class RoupaAluguelCompra extends javax.swing.JFrame {

    private final Date dataPedido = new Date();
    private Pedido pedido = new Pedido(dataPedido);
    private Roupa roupa = new Roupa();
    private PedidoDAO dao = new PedidoDAO();
    private ClienteDAO clienteDAO = new ClienteDAO();
    private Cliente cliente;

    private final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    public RoupaAluguelCompra() {
        initComponents();
        this.centralizarTela();
        this.preencherFormulario();
        EntityManager em = JPAUtil.getEntityManager();
    }

    public RoupaAluguelCompra(Pedido pedido) {
        this.pedido = pedido;
        this.centralizarTela();
        this.preencherFormulario();
    }

    private void preencherFormulario() {

        this.campoCodigoPedido.setText(String.valueOf(this.pedido.getId()));

        String dataAtual = df.format(this.pedido.getDataPedido());
        this.campoDataPedido.setText(dataAtual);

        this.campoCodigoCliente.setText(String.valueOf(this.pedido.getCliente().getId()));
        this.campoCPF.setText((String) this.pedido.getCliente().getCPF());
        this.campoNome.setText((String) this.pedido.getCliente().getNome());
//        this.campoValorAdiantado.setText((String) this.pedido.getValorAdiantado());
        //       this.campoValorTotal.setText((String) this.pedido.getValorTotal());

        this.campoNome.requestFocus();
    }

    private Pedido getItemSelecionado() {
        int row = this.tabelaPedido.getSelectedRow();
        String tipo = (String) this.tabelaPedido.getModel().getValueAt(row, 0);
        int quantidade = (int) this.tabelaPedido.getModel().getValueAt(row, 1);
        Date dataPedido = (Date) this.tabelaPedido.getModel().getValueAt(row, 2);
        Date dataEntrega = (Date) this.tabelaPedido.getModel().getValueAt(row, 3);
        String descricao = (String) this.tabelaPedido.getModel().getValueAt(row, 4);
        String valorUnitario = (String) this.tabelaPedido.getModel().getValueAt(row, 5);

        Pedido p = new Pedido();
         //           p.setItens(tipo);
        //          p.setQuantidad(quantidade);
        p.setDataPedido(dataPedido);
         //           p.setDataEntrega(dataEntrega);
        //         p.setDescricao(descricao);
        //         p.setValorReceber(valorUnitario);
        return p;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popUpMenu = new javax.swing.JPopupMenu();
        menuEditar = new javax.swing.JMenuItem();
        separador = new javax.swing.JPopupMenu.Separator();
        menuRemover = new javax.swing.JMenuItem();
        grupoTipo = new javax.swing.ButtonGroup();
        jLabel11 = new javax.swing.JLabel();
        campoCodigoPedido = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        campoCodigoCliente = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        campoCPF = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        campoNome = new javax.swing.JTextField();
        botaoPesquisarCliente = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        botaoNovoPedido = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        campoCodigoRoupa = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        campoDescricao = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        comboDisponibilidade = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        spinQuantidade = new javax.swing.JSpinner();
        radioAluguel = new javax.swing.JRadioButton();
        radioCompra = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        campoDataEntrega = new javax.swing.JFormattedTextField();
        campoValorUnitario = new javax.swing.JFormattedTextField();
        image = new javax.swing.JLabel();
        botaoCancelar = new javax.swing.JButton();
        botaoRealizarPedido = new javax.swing.JButton();
        campoValorAdiantado = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        campoValorTotal = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPedido = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        campoDataPedido = new javax.swing.JFormattedTextField();

        menuEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/edit.png"))); // NOI18N
        menuEditar.setText("Editar");
        menuEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEditarActionPerformed(evt);
            }
        });
        popUpMenu.add(menuEditar);
        popUpMenu.add(separador);

        menuRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/delete.png"))); // NOI18N
        menuRemover.setText("Remover");
        menuRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverActionPerformed(evt);
            }
        });
        popUpMenu.add(menuRemover);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Realizar Pedido");

        jLabel11.setText("Código:");

        campoCodigoPedido.setEditable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados do Cliente"));

        jLabel2.setText("Código:");

        campoCodigoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoCodigoClienteActionPerformed(evt);
            }
        });

        jLabel4.setText("CPF:");

        try {
            campoCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Nome:");

        botaoPesquisarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N
        botaoPesquisarCliente.setText("Pesquisar");
        botaoPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoPesquisarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(24, 24, 24)
                        .addComponent(campoNome))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(campoCodigoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(campoCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(botaoPesquisarCliente)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(campoCodigoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(campoCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(campoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoPesquisarCliente))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Pedido"));

        botaoNovoPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/add.png"))); // NOI18N
        botaoNovoPedido.setText("Novo Pedido");
        botaoNovoPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoNovoPedidoActionPerformed(evt);
            }
        });

        jLabel1.setText("Código:");

        campoCodigoRoupa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoCodigoRoupaActionPerformed(evt);
            }
        });

        jLabel5.setText("Pesquisa:");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N

        jLabel8.setText("Disponibilidade:");

        comboDisponibilidade.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel9.setText("Quantidade:");

        jLabel10.setText("Valor R$:");

        grupoTipo.add(radioAluguel);
        radioAluguel.setText("Aluguel");
        radioAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAluguelActionPerformed(evt);
            }
        });

        grupoTipo.add(radioCompra);
        radioCompra.setText("Compra");

        jLabel12.setText("Data Entrega:");

        try {
            campoDataEntrega.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        image.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/camera.png"))); // NOI18N
        image.setBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(255, 255, 255)));
        image.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imageMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(campoCodigoRoupa, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(campoDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(image, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(botaoNovoPedido))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(radioAluguel)
                                        .addGap(18, 18, 18)
                                        .addComponent(radioCompra))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(campoValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel12)
                                        .addGap(18, 18, 18)
                                        .addComponent(campoDataEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(spinQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(comboDisponibilidade, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(campoCodigoRoupa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5)
                        .addComponent(campoDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton2))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(comboDisponibilidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spinQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(radioAluguel)
                    .addComponent(radioCompra))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12)
                    .addComponent(campoDataEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(campoValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botaoNovoPedido)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(image)
                .addGap(25, 25, 25))
        );

        botaoCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/cancel.png"))); // NOI18N
        botaoCancelar.setText("Cancelar");
        botaoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelarActionPerformed(evt);
            }
        });

        botaoRealizarPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/check.png"))); // NOI18N
        botaoRealizarPedido.setText("Realizar Pedido");

        campoValorAdiantado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        jLabel6.setText("Valor Adiantado:");

        jLabel7.setText("Valor Total:");

        campoValorTotal.setEditable(false);

        tabelaPedido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tabelaPedido);

        jLabel13.setText("Data Pedido:");

        campoDataPedido.setEditable(false);
        try {
            campoDataPedido.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(campoCodigoPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(238, 238, 238)
                        .addComponent(jLabel13)
                        .addGap(18, 18, 18)
                        .addComponent(campoDataPedido))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(327, 327, 327)
                                .addComponent(botaoRealizarPedido)
                                .addGap(18, 18, 18)
                                .addComponent(botaoCancelar))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(campoValorAdiantado, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(campoValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(campoDataPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(campoCodigoPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(campoValorAdiantado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(campoValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoCancelar)
                    .addComponent(botaoRealizarPedido))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void campoCodigoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoCodigoClienteActionPerformed

    }//GEN-LAST:event_campoCodigoClienteActionPerformed

    private void botaoNovoPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoNovoPedidoActionPerformed

    }//GEN-LAST:event_botaoNovoPedidoActionPerformed

    private void botaoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_botaoCancelarActionPerformed

    private void menuEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEditarActionPerformed
        this.pedido = this.getItemSelecionado();
        this.preencherFormulario();
    }//GEN-LAST:event_menuEditarActionPerformed

    private void menuRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverActionPerformed

        Pedido p = this.getItemSelecionado();

        int dialogResult = JOptionPane.showConfirmDialog(this, "Deseja realmente remover o item selecionado ?",
                "Exclusão",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
            try {
                dao.delete(p);
                if (this.pedido.getId() == p.getId()) {
                    this.novo();
                }

                this.atulizarTabela();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Falha ao remover\nExistem clientes utilizando a roupa!!!", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_menuRemoverActionPerformed

    private void campoCodigoRoupaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoCodigoRoupaActionPerformed
    }//GEN-LAST:event_campoCodigoRoupaActionPerformed

    private void radioAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAluguelActionPerformed
    }//GEN-LAST:event_radioAluguelActionPerformed

    private void imageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imageMouseClicked

   //     int returnVal = this.escolherImagem.showDialog(this, "OK");

        /*if (returnVal == JFileChooser.APPROVE_OPTION) {
         File file;
         file = fc.getSelectedFile();
         log.append("Attaching file: " + file.getName()
         + "." + newline);
         } else {
         log.append("Attachment cancelled by user." + newline);
         }
         log.setCaretPosition(log.getDocument().getLength());

         fc.setSelectedFile(null);*/
    }//GEN-LAST:event_imageMouseClicked

    private void botaoPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoPesquisarClienteActionPerformed
        this.pesquisar();        // TODO add your handling code here:
    }//GEN-LAST:event_botaoPesquisarClienteActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RoupaAluguelCompra().setVisible(true);
            }
        });
    }

    public void centralizarTela() {
        this.setLocationRelativeTo(null);
    }

    private Pedido preencherObjeto() {

        Pedido pedido = new Pedido(this.dataPedido);

        pedido.setId(Integer.parseInt(this.campoCodigoPedido.getText()));
        pedido.getCliente().setId(Integer.parseInt(this.campoCodigoPedido.getText()));
        pedido.getCliente().setCPF((String) this.campoCPF.getText());
        pedido.getCliente().setNome((String) this.campoNome.getText());
    //    pedido.setValorAdiantado((String) this.campoValorAdiantado.getText());
        //   pedido.setValorTotal((String) this.campoValorTotal.getText());

        return pedido;
    }

    public void atulizarTabela() {

        List<Pedido> lista = dao.findAll();
        String[] col = new String[]{"Tipo", "Quantidade", "Data Pedido", "Data Entrega", "Descrição", "Valor Unitario"};
        DefaultTableModel modelo = new DefaultTableModel(null, col) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Pedido p : lista) {
            Object[] row = new Object[6];
        //    row[0] = p.getTipo();
            //   row[1] = p.getQuantidade();
            row[2] = p.getDataPedido();
        //    row[3] = p.getDataEntrega();
            //   row[4] = p.getValorReceber();
            modelo.addRow(row);

        }
        this.tabelaPedido.setModel(modelo);
    }

    private void salvar() {
        try {

            this.preencherObjeto();

            if (this.pedido.getId() == 0) {
                dao.save(this.pedido);
                JOptionPane.showMessageDialog(this, "Salvo com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
            } else {
                dao.update(pedido);
                JOptionPane.showMessageDialog(this, "Atualizado com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Falha ao Salvar!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        }

        this.preencherFormulario();

        this.atulizarTabela();

    }

    private void novo() {
        this.pedido = new Pedido();
        this.preencherFormulario();
    }

    private void preencherCliente() {
        if (this.cliente != null) {
            this.campoNome.setText(this.cliente.getNome());
            this.campoCodigoCliente.setText(String.valueOf(this.cliente.getId()));
            this.campoCPF.setText(this.cliente.getCPF());
        }
    }

    private void pesquisar() {

        int id = 0;

        if (!this.campoCodigoCliente.getText().equals("")) {
            id = (Integer.parseInt(this.campoCodigoCliente.getText()));
        }
        String CPF = "";
        if (!this.campoCPF.getText().replace(".", "").replace("-", "").replace(" ", "").equals("")) {
            CPF = ((String) this.campoCPF.getText());
        }
        String nome = ((String) this.campoNome.getText());

        List<Cliente> lista = clienteDAO.findBYUnit(id, CPF, nome);

        if (lista.size() > 0) {
            this.cliente = lista.get(0);
        } else {
            JOptionPane.showMessageDialog(this, "Nao Foram encontados resultados", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        this.preencherCliente();

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoCancelar;
    private javax.swing.JButton botaoNovoPedido;
    private javax.swing.JButton botaoPesquisarCliente;
    private javax.swing.JButton botaoRealizarPedido;
    private javax.swing.JFormattedTextField campoCPF;
    private javax.swing.JTextField campoCodigoCliente;
    private javax.swing.JTextField campoCodigoPedido;
    private javax.swing.JTextField campoCodigoRoupa;
    private javax.swing.JFormattedTextField campoDataEntrega;
    private javax.swing.JFormattedTextField campoDataPedido;
    private javax.swing.JTextField campoDescricao;
    private javax.swing.JTextField campoNome;
    private javax.swing.JFormattedTextField campoValorAdiantado;
    private javax.swing.JTextField campoValorTotal;
    private javax.swing.JFormattedTextField campoValorUnitario;
    private javax.swing.JComboBox comboDisponibilidade;
    private javax.swing.ButtonGroup grupoTipo;
    private javax.swing.JLabel image;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem menuEditar;
    private javax.swing.JMenuItem menuRemover;
    private javax.swing.JPopupMenu popUpMenu;
    private javax.swing.JRadioButton radioAluguel;
    private javax.swing.JRadioButton radioCompra;
    private javax.swing.JPopupMenu.Separator separador;
    private javax.swing.JSpinner spinQuantidade;
    private javax.swing.JTable tabelaPedido;
    // End of variables declaration//GEN-END:variables
}
