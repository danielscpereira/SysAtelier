package SysAtelier.views;

public class Principal extends javax.swing.JFrame {

    private CadastrarCliente cadastrarCliente;
    private CadastrarRoupa cadastrarPeca;
    private RoupaAluguelCompra cadastrarPedido;
    private PesquisarCliente pesquisarCliente;
    private PesquisarRoupa pesquisarPeca;
    private PesquisarPedido pesquisarPedido;
    private RoupaSobMedida sobMedida;
    
    public Principal() {
        initComponents();
        this.centralizarTela(); 
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        cadastro = new javax.swing.JMenu();
        cadastroRoupa = new javax.swing.JMenuItem();
        cadastroCliente = new javax.swing.JMenuItem();
        pedido = new javax.swing.JMenu();
        cadastroPedido = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        consulta = new javax.swing.JMenu();
        consultaRoupa = new javax.swing.JMenuItem();
        consultaCliente = new javax.swing.JMenuItem();
        consultaPedido = new javax.swing.JMenuItem();
        relatorio = new javax.swing.JMenu();
        emitirRelatorio = new javax.swing.JMenuItem();
        ajuda = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bem-vindo ao SysAtelier");

        cadastro.setText("Cadastro");

        cadastroRoupa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/add.png"))); // NOI18N
        cadastroRoupa.setText("Cadastrar Roupa");
        cadastroRoupa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroRoupaActionPerformed(evt);
            }
        });
        cadastro.add(cadastroRoupa);

        cadastroCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/add.png"))); // NOI18N
        cadastroCliente.setText("Cadastrar Cliente");
        cadastroCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroClienteActionPerformed(evt);
            }
        });
        cadastro.add(cadastroCliente);

        jMenuBar1.add(cadastro);

        pedido.setText("Pedidos");

        cadastroPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/cash.png"))); // NOI18N
        cadastroPedido.setText("Aluguel/Compra");
        cadastroPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroPedidoActionPerformed(evt);
            }
        });
        pedido.add(cadastroPedido);

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/dimension.png"))); // NOI18N
        jMenuItem1.setText("Sob Medida");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        pedido.add(jMenuItem1);

        jMenuBar1.add(pedido);

        consulta.setText("Consulta");

        consultaRoupa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N
        consultaRoupa.setText("Pesquisar Roupa");
        consultaRoupa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultaRoupaActionPerformed(evt);
            }
        });
        consulta.add(consultaRoupa);

        consultaCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N
        consultaCliente.setText("Pesquisar Cliente");
        consultaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultaClienteActionPerformed(evt);
            }
        });
        consulta.add(consultaCliente);

        consultaPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N
        consultaPedido.setText("Pesquisar Pedido");
        consultaPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultaPedidoActionPerformed(evt);
            }
        });
        consulta.add(consultaPedido);

        jMenuBar1.add(consulta);

        relatorio.setText("Relatórios");

        emitirRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/text.png"))); // NOI18N
        emitirRelatorio.setText("Emitir Relatório");
        relatorio.add(emitirRelatorio);

        jMenuBar1.add(relatorio);

        ajuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/earth.png"))); // NOI18N
        ajuda.setText("Ajuda");
        jMenuBar1.add(ajuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 256, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void consultaPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultaPedidoActionPerformed
        this.pesquisarPedido();
    }//GEN-LAST:event_consultaPedidoActionPerformed

    private void consultaRoupaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultaRoupaActionPerformed
        this.pesquisarPeca();
    }//GEN-LAST:event_consultaRoupaActionPerformed

    private void consultaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultaClienteActionPerformed
        this.pesquisarCliente();
    }//GEN-LAST:event_consultaClienteActionPerformed

    private void cadastroPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroPedidoActionPerformed
        this.cadastrarPedido();
    }//GEN-LAST:event_cadastroPedidoActionPerformed

    private void cadastroRoupaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroRoupaActionPerformed
        this.cadastrarPeca();
    }//GEN-LAST:event_cadastroRoupaActionPerformed

    private void cadastroClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroClienteActionPerformed
        this.cadastrarCliente();
    }//GEN-LAST:event_cadastroClienteActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.sobMedida();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

  
    public static void main(String args[]) {
    
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }
    
    public void centralizarTela(){
        this.setLocationRelativeTo(null);
    }
    
    public void cadastrarCliente(){
       this.cadastrarCliente = new CadastrarCliente();
       this.cadastrarCliente.setVisible(true);
    }
    
    public void cadastrarPeca(){
       this.cadastrarPeca = new CadastrarRoupa();
       this.cadastrarPeca.setVisible(true);
    }
    
    public void cadastrarPedido(){
       this.cadastrarPedido = new RoupaAluguelCompra();
       this.cadastrarPedido.setVisible(true);
    }

    public void pesquisarCliente(){
       this.pesquisarCliente = new PesquisarCliente();
       this.pesquisarCliente.setVisible(true);
    }
    
    public void pesquisarPeca(){
       this.pesquisarPeca = new PesquisarRoupa();
       this.pesquisarPeca.setVisible(true);
    }
    
    public void pesquisarPedido(){
       this.pesquisarPedido = new PesquisarPedido();
       this.pesquisarPedido.setVisible(true);
    }
    
    public void sobMedida(){
      this.sobMedida = new RoupaSobMedida();
      this.sobMedida.setVisible(true);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu ajuda;
    private javax.swing.JMenu cadastro;
    private javax.swing.JMenuItem cadastroCliente;
    private javax.swing.JMenuItem cadastroPedido;
    private javax.swing.JMenuItem cadastroRoupa;
    private javax.swing.JMenu consulta;
    private javax.swing.JMenuItem consultaCliente;
    private javax.swing.JMenuItem consultaPedido;
    private javax.swing.JMenuItem consultaRoupa;
    private javax.swing.JMenuItem emitirRelatorio;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenu pedido;
    private javax.swing.JMenu relatorio;
    // End of variables declaration//GEN-END:variables
}
