package SysAtelier.views;

import SysAtelier.dao.JPAUtil;
import SysAtelier.dao.RoupaDAO;
import SysAtelier.model.Roupa;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;

public final class CadastrarRoupa extends javax.swing.JFrame {
    
    private Roupa roupa = new Roupa();
    private RoupaDAO dao = new RoupaDAO();
    private JFileChooser fc = new JFileChooser();
    private String tamanho;

    public CadastrarRoupa() {
        initComponents();
        this.centralizarTela();
        this.preencherFormulario();
        EntityManager em = JPAUtil.getEntityManager();
    }
    
    public CadastrarRoupa(Roupa roupa) {
        this.roupa = roupa;
        this.centralizarTela();
        this.preencherFormulario();
    }
    
    private void preencherFormulario() {
        this.campoCodigo.setText(String.valueOf(this.roupa.getId()));
        
        if (this.comboGenero.getSelectedItem().equals("")){
            this.comboGenero.setSelectedIndex(0);
        } else {
            this.comboGenero.setSelectedItem(this.roupa.getGeneroFantasia());
        }  
        if (this.comboTipo.equals("")){
            this.comboTipo.setSelectedIndex(0);
        } else {
            this.comboTipo.setSelectedItem(this.roupa.getTipoFantasia());
        }  
        this.spinQuantidade.setValue(this.roupa.getQuantidade());
        //this.grupoTamanho.setSelected(null, false);
        this.campoDescricao.setText(this.roupa.getDescricao());
        
        this.campoDescricao.requestFocus();
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoTamanho = new javax.swing.ButtonGroup();
        escolherImagem = new javax.swing.JFileChooser();
        popUpMenu = new javax.swing.JPopupMenu();
        menuEditar = new javax.swing.JMenuItem();
        separador = new javax.swing.JPopupMenu.Separator();
        menuRemover = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboGenero = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        comboTipo = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        radioPP = new javax.swing.JRadioButton();
        radioP = new javax.swing.JRadioButton();
        radioM = new javax.swing.JRadioButton();
        radioG = new javax.swing.JRadioButton();
        radioGG = new javax.swing.JRadioButton();
        escritaPP = new javax.swing.JLabel();
        escritaP = new javax.swing.JLabel();
        escritaM = new javax.swing.JLabel();
        escritaG = new javax.swing.JLabel();
        escritaGG = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        spinQuantidade = new javax.swing.JSpinner();
        campoDescricao = new javax.swing.JTextField();
        botaoCancelar = new javax.swing.JButton();
        botaoCadastrar = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        campoCodigo = new javax.swing.JTextField();
        botaoLimpaDados = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaFantasias = new javax.swing.JTable();
        image = new javax.swing.JLabel();

        menuEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/edit.png"))); // NOI18N
        menuEditar.setText("Editar");
        menuEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEditarActionPerformed(evt);
            }
        });
        popUpMenu.add(menuEditar);
        popUpMenu.add(separador);

        menuRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/delete.png"))); // NOI18N
        menuRemover.setText("Remover");
        menuRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverActionPerformed(evt);
            }
        });
        popUpMenu.add(menuRemover);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastramento de Roupa");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("Dados do Aluguel"), "Dados da Fantasia"));

        jLabel1.setText("Gênero da Fantasia:");

        comboGenero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Feminino" }));

        jLabel2.setText("Tipo da Fantasia:");

        comboTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Adulto", "Infantil" }));
        comboTipo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboTipoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                comboTipoFocusLost(evt);
            }
        });
        comboTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTipoActionPerformed(evt);
            }
        });

        jLabel3.setText("Tamanho:");

        grupoTamanho.add(radioPP);
        radioPP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioPPActionPerformed(evt);
            }
        });

        grupoTamanho.add(radioP);
        radioP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioPActionPerformed(evt);
            }
        });

        grupoTamanho.add(radioM);
        radioM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioMActionPerformed(evt);
            }
        });

        grupoTamanho.add(radioG);
        radioG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioGActionPerformed(evt);
            }
        });

        grupoTamanho.add(radioGG);
        radioGG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioGGActionPerformed(evt);
            }
        });

        escritaPP.setText(" PP");

        escritaP.setText("  P");

        escritaM.setText("  M");

        escritaG.setText("  G");

        escritaGG.setText(" GG");

        jLabel9.setText("Descrição da Fantasia:");

        jLabel10.setText("Quantidade:");

        spinQuantidade.setModel(new SpinnerNumberModel (0, 0, null, 1));
        spinQuantidade.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                spinQuantidadeFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioPP)
                            .addComponent(escritaPP))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioP)
                            .addComponent(escritaP))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioM)
                            .addComponent(escritaM))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioG)
                            .addComponent(escritaG))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(escritaGG)
                            .addComponent(radioGG)))
                    .addComponent(jLabel9)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(comboTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboGenero, 0, 127, Short.MAX_VALUE)
                            .addComponent(spinQuantidade)))
                    .addComponent(campoDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboGenero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(spinQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioPP)
                            .addComponent(jLabel3))
                        .addComponent(radioP))
                    .addComponent(radioM)
                    .addComponent(radioG)
                    .addComponent(radioGG))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(escritaPP)
                    .addComponent(escritaP)
                    .addComponent(escritaM)
                    .addComponent(escritaG)
                    .addComponent(escritaGG))
                .addGap(25, 25, 25)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(campoDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botaoCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/cancel.png"))); // NOI18N
        botaoCancelar.setText("Cancelar");
        botaoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelarActionPerformed(evt);
            }
        });

        botaoCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/check.png"))); // NOI18N
        botaoCadastrar.setText("Cadastrar");
        botaoCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCadastrarActionPerformed(evt);
            }
        });

        jLabel11.setText("Código:");

        campoCodigo.setEditable(false);

        botaoLimpaDados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/clean.png"))); // NOI18N
        botaoLimpaDados.setText("Limpar Dados");
        botaoLimpaDados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoLimpaDadosActionPerformed(evt);
            }
        });

        tabelaFantasias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tabelaFantasias.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaFantasiasMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tabelaFantasiasMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaFantasias);

        image.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/camera.png"))); // NOI18N
        image.setBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(255, 255, 255)));
        image.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imageMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botaoLimpaDados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botaoCadastrar)
                        .addGap(18, 18, 18)
                        .addComponent(botaoCancelar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(campoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 382, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(image, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(campoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(image, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoCancelar)
                    .addComponent(botaoCadastrar)
                    .addComponent(botaoLimpaDados))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radioPPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioPPActionPerformed
        this.tamanho="PP";
    }//GEN-LAST:event_radioPPActionPerformed

    private void botaoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelarActionPerformed
        this.dispose(); 
    }//GEN-LAST:event_botaoCancelarActionPerformed

    private void botaoLimpaDadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoLimpaDadosActionPerformed
        this.novo();
    }//GEN-LAST:event_botaoLimpaDadosActionPerformed

    private void imageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imageMouseClicked

        int returnVal = this.escolherImagem.showDialog(this, "OK");

        if (fc.showOpenDialog(image) == JFileChooser.APPROVE_OPTION){
            
            java.io.File f = fc.getSelectedFile();
            
            image.setIcon(new ImageIcon(f.toString()));
            
            image.setHorizontalAlignment(image.CENTER);
            
        } else {
            JOptionPane.showMessageDialog(null, "Voce nao selecionou nenhuma imagem.");   
        } 
        
        /*if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file;
            file = fc.getSelectedFile();
            log.append("Attaching file: " + file.getName()
                       + "." + newline);
        } else {
            log.append("Attachment cancelled by user." + newline);
        }
        log.setCaretPosition(log.getDocument().getLength());

        fc.setSelectedFile(null);*/
    }//GEN-LAST:event_imageMouseClicked

    private void botaoCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCadastrarActionPerformed
        this.salvar();
    }//GEN-LAST:event_botaoCadastrarActionPerformed
    
    private void tabelaFantasiasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaFantasiasMouseClicked
        if (evt.getClickCount() > 1) {
            this.roupa = this.getItemSelecionado();
            this.preencherFormulario();
        }
    }//GEN-LAST:event_tabelaFantasiasMouseClicked
    
    private void tabelaFantasiasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaFantasiasMouseReleased
        if (evt.getButton() == 3 && this.tabelaFantasias.getSelectedRow() != -1) {
            popUpMenu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_tabelaFantasiasMouseReleased
    private Roupa getItemSelecionado() {
                    int row = this.tabelaFantasias.getSelectedRow();
                    int id = (int) this.tabelaFantasias.getModel().getValueAt(row, 0);
                    String generoFantasia = (String) this.tabelaFantasias.getModel().getValueAt(row, 1);
                    String tipoFantasia = (String) this.tabelaFantasias.getModel().getValueAt(row, 2);
                    int quantidade = (int) this.tabelaFantasias.getModel().getValueAt(row, 3);
                    String tamanho = (String) this.tabelaFantasias.getModel().getValueAt(row, 4);
                    String descricao = (String) this.tabelaFantasias.getModel().getValueAt(row, 5);

                    Roupa r = new Roupa();
                    r.setId(id);
                    r.setGeneroFantasia(generoFantasia);
                    r.setTipoFantasia(tipoFantasia);
                    r.setQuantidade(quantidade);
                    r.setTamanho(tamanho);
                    r.setDescricao(descricao);
                    return r;
            }
    private void menuEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEditarActionPerformed
        this.roupa = this.getItemSelecionado();
        this.preencherFormulario();
    }//GEN-LAST:event_menuEditarActionPerformed

    private void menuRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverActionPerformed

        Roupa r = this.getItemSelecionado();

        int dialogResult = JOptionPane.showConfirmDialog(this, "Deseja realmente remover o item selecionado ?",
            "Exclusão",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
            try {
                dao.delete(r);
                if (this.roupa.getId() == r.getId()) {
                    this.novo();
                }

                this.atulizarTabela();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Falha ao remover\nExistem clientes utilizando a roupa!!!", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_menuRemoverActionPerformed

    private void spinQuantidadeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_spinQuantidadeFocusGained
        SpinnerNumberModel model = new SpinnerNumberModel(0, 0, null, 1);  
    }//GEN-LAST:event_spinQuantidadeFocusGained

    private void comboTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTipoActionPerformed
        
    }//GEN-LAST:event_comboTipoActionPerformed

    private void radioPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioPActionPerformed
        this.tamanho="P";
    }//GEN-LAST:event_radioPActionPerformed

    private void radioMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioMActionPerformed
        this.tamanho="M";
    }//GEN-LAST:event_radioMActionPerformed

    private void radioGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioGActionPerformed
        this.tamanho="G";
    }//GEN-LAST:event_radioGActionPerformed

    private void radioGGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioGGActionPerformed
        this.tamanho="GG";
    }//GEN-LAST:event_radioGGActionPerformed

    private void comboTipoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboTipoFocusLost
        if (this.comboTipo.getSelectedItem().equals("Adulto")){
            this.radioPP.setEnabled(false);
            this.radioPP.setSelected(false);
        } else {
            this.radioPP.setEnabled(true);
        }        
    }//GEN-LAST:event_comboTipoFocusLost

    private void comboTipoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboTipoFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_comboTipoFocusGained
    
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastrarRoupa().setVisible(true);
            }
        });
    }
    
    public void centralizarTela(){
        this.setLocationRelativeTo(null);
    }
    
    
    
    private Roupa preencherObjeto() {
        Roupa roupa = new Roupa();

        roupa.setId(Integer.parseInt(this.campoCodigo.getText()));
        roupa.setGeneroFantasia((String) this.comboGenero.getSelectedItem());
        roupa.setTipoFantasia((String) this.comboTipo.getSelectedItem());
        roupa.setQuantidade((Integer)this.spinQuantidade.getValue()) ; 
        roupa.setTamanho(String.valueOf(this.grupoTamanho.getSelection()));
        roupa.setDescricao((String) this.campoDescricao.getText());
        
        return roupa;
    }
    
    public void atulizarTabela() {

        List<Roupa> lista = dao.findAll();
        String[] col = new String[]{"Cód.", "Gênero", "Tipo", "Quantidade", "Tamanho", "Descrição"};
        DefaultTableModel modelo = new DefaultTableModel(null, col) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Roupa r : lista) {
            Object[] row = new Object[6];
            row[0] = r.getId();
            row[1] = r.getGeneroFantasia();
            row[2] = r.getTipoFantasia();
            row[3] = r.getQuantidade();
            row[4] = r.getTamanho();
            row[5] = r.getDescricao();
            modelo.addRow(row);

        }
        this.tabelaFantasias.setModel(modelo);
    }
    
    private void salvar() {
        try {

            this.roupa = this.preencherObjeto();

            try {
                if (this.roupa.getId() == 0) {
                    dao.save(this.roupa);
                    this.preencherFormulario();
                    this.atulizarTabela();

                    JOptionPane.showMessageDialog(this, "Salvo com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    dao.update(roupa);
                    this.preencherFormulario();
                    this.atulizarTabela();
                    JOptionPane.showMessageDialog(this, "Atualizado com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Falha ao Salvar!!!", "Erro", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Favor preencher todos os campos!!!", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    private void novo() {
        this.roupa = new Roupa();
        this.preencherFormulario();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoCadastrar;
    private javax.swing.JButton botaoCancelar;
    private javax.swing.JButton botaoLimpaDados;
    private javax.swing.JTextField campoCodigo;
    private javax.swing.JTextField campoDescricao;
    private javax.swing.JComboBox comboGenero;
    private javax.swing.JComboBox comboTipo;
    private javax.swing.JFileChooser escolherImagem;
    private javax.swing.JLabel escritaG;
    private javax.swing.JLabel escritaGG;
    private javax.swing.JLabel escritaM;
    private javax.swing.JLabel escritaP;
    private javax.swing.JLabel escritaPP;
    private javax.swing.ButtonGroup grupoTamanho;
    private javax.swing.JLabel image;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem menuEditar;
    private javax.swing.JMenuItem menuRemover;
    private javax.swing.JPopupMenu popUpMenu;
    private javax.swing.JRadioButton radioG;
    private javax.swing.JRadioButton radioGG;
    private javax.swing.JRadioButton radioM;
    private javax.swing.JRadioButton radioP;
    private javax.swing.JRadioButton radioPP;
    private javax.swing.JPopupMenu.Separator separador;
    private javax.swing.JSpinner spinQuantidade;
    private javax.swing.JTable tabelaFantasias;
    // End of variables declaration//GEN-END:variables
}
