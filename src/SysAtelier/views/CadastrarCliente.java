package SysAtelier.views;

import SysAtelier.dao.ClienteDAO;
import SysAtelier.dao.JPAUtil;
import SysAtelier.model.Cliente;
import SysAtelier.model.Endereco;
import SysAtelier.webService.WebServiceCep;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class CadastrarCliente extends javax.swing.JFrame {

    private final Date dataCadastro = new Date();
    private Cliente cliente = new Cliente(dataCadastro);
    private Endereco endereco = new Endereco();
    private ClienteDAO dao = new ClienteDAO();

    private final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    public CadastrarCliente() {
        initComponents();
        this.centralizarTela();
        this.preencherFormulario();
        EntityManager em = JPAUtil.getEntityManager();
        this.atulizarTabela();
    }

    public CadastrarCliente(Cliente cliente) {
        this.cliente = cliente;
        this.centralizarTela();
        this.preencherFormulario();
        this.atulizarTabela();
    }

    private void preencherFormulario() {

        String dataAtual = df.format(this.cliente.getDataCadastro());
        this.campoDataCadastro.setText(dataAtual);

        this.campoCodigo.setText(String.valueOf(this.cliente.getId()));
        this.campoNome.setText(this.cliente.getNome());
        this.campoCPF.setText(String.valueOf(this.cliente.getCPF()));
        this.campoRG.setText(this.cliente.getRG());
        this.campoTelFixo.setText(String.valueOf(this.cliente.getTelFixo()));
        this.campoTelCelular.setText(String.valueOf(this.cliente.getTelCelular()));
        if (this.comboSexo.getSelectedItem() == null || this.comboSexo.getSelectedItem().equals("")) {
            this.comboSexo.setSelectedItem("Masculino");
        } else {
            this.comboSexo.setSelectedItem(this.cliente.getSexo());
        }
        this.campoEmail.setText(this.cliente.getEmail());
        this.campoCEP.setText(String.valueOf(this.endereco.getCEP()));
        this.campoLogradouro.setText(this.endereco.getLogradouro());
        this.campoNumero.setText(this.endereco.getNumero() == 0 ? "" : String.valueOf(this.endereco.getNumero()));
        this.campoComplemento.setText(this.endereco.getComplemento());
        this.campoBairro.setText(this.endereco.getBairro());
        this.campoCidade.setText(this.endereco.getCidade());
        if (this.comboEstado.getSelectedItem() == null || this.comboEstado.getSelectedItem().equals("")){
            this.comboEstado.setSelectedItem("ES");
        } else {
            this.comboEstado.setSelectedItem(this.endereco.getUF());
        }
        
        this.campoNome.requestFocus();
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popUpMenu = new javax.swing.JPopupMenu();
        menuEditar = new javax.swing.JMenuItem();
        separador = new javax.swing.JPopupMenu.Separator();
        menuRemover = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        campoNome = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        campoCPF = new javax.swing.JFormattedTextField();
        campoTelCelular = new javax.swing.JFormattedTextField();
        campoRG = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        campoTelFixo = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        comboSexo = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        campoEmail = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        campoCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        campoCEP = new javax.swing.JFormattedTextField();
        botaoBuscarCEP = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        campoLogradouro = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        campoNumero = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        campoComplemento = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        campoBairro = new javax.swing.JTextField();
        campoCidade = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        comboEstado = new javax.swing.JComboBox();
        botaoCancelar = new javax.swing.JButton();
        botaoSalvar = new javax.swing.JButton();
        botaoLimparDados = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaClientes = new javax.swing.JTable();
        campoDataCadastro = new javax.swing.JFormattedTextField();

        menuEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/edit.png"))); // NOI18N
        menuEditar.setText("Editar");
        menuEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEditarActionPerformed(evt);
            }
        });
        popUpMenu.add(menuEditar);
        popUpMenu.add(separador);

        menuRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/delete.png"))); // NOI18N
        menuRemover.setText("Remover");
        menuRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverActionPerformed(evt);
            }
        });
        popUpMenu.add(menuRemover);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastramento de Cliente");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Pessoais"));

        jLabel3.setText("Nome:");

        campoNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoNomeActionPerformed(evt);
            }
        });

        jLabel4.setText("CPF:");

        jLabel5.setText("RG:");

        try {
            campoCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            campoTelCelular.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) 9####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        campoTelCelular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoTelCelularActionPerformed(evt);
            }
        });

        jLabel6.setText("Tel. Fixo:");

        jLabel7.setText("Tel. Celular:");

        try {
            campoTelFixo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) ####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        campoTelFixo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoTelFixoActionPerformed(evt);
            }
        });

        jLabel8.setText("Sexo:");

        comboSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Feminino" }));
        comboSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSexoActionPerformed(evt);
            }
        });

        jLabel9.setText("E-mail:");

        campoEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoEmailActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(32, 32, 32)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(campoCPF)
                            .addComponent(comboSexo, 0, 104, Short.MAX_VALUE)
                            .addComponent(campoTelFixo))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(campoRG, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(campoTelCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(campoEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(campoNome))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(campoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(campoCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(campoRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(campoTelFixo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(campoTelCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(comboSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(campoEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Código:");

        campoCodigo.setEditable(false);

        jLabel2.setText("Data de Cadastro:");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Endereço"));

        jLabel10.setText("CEP:");

        try {
            campoCEP.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        botaoBuscarCEP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N
        botaoBuscarCEP.setText("Buscar CEP");
        botaoBuscarCEP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoBuscarCEPActionPerformed(evt);
            }
        });

        jLabel11.setText("Logradouro:");

        campoLogradouro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoLogradouroActionPerformed(evt);
            }
        });

        jLabel12.setText("Número:");

        campoNumero.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("###"))));

        jLabel13.setText("Complemento:");

        jLabel14.setText("Bairro:");

        jLabel15.setText("Cidade:");

        jLabel16.setText("Estado:");

        comboEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC ", "AL ", "AP ", "AM ", "BA ", "CE ", "DF ", "ES ", "MA ", "MT ", "MS ", "MG ", "PA ", "PB ", "PR ", "PE ", "PI ", "RJ ", "RN ", "RS ", "RO ", "RR ", "SC ", "SP ", "SE ", "TO" }));
        comboEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboEstadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel15)
                            .addGap(42, 42, 42)
                            .addComponent(campoCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(165, 165, 165))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(20, 20, 20)
                                .addComponent(campoLogradouro))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(226, 226, 226)
                                .addComponent(campoComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                            .addGap(79, 79, 79)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(campoNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(30, 30, 30)
                                    .addComponent(jLabel13)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel16)
                                    .addGap(14, 14, 14)
                                    .addComponent(comboEstado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addComponent(campoBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(campoCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(botaoBuscarCEP)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(campoCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoBuscarCEP))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(campoLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(campoComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(campoNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(campoBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(campoCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(comboEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botaoCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/cancel.png"))); // NOI18N
        botaoCancelar.setText("Cancelar");
        botaoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelarActionPerformed(evt);
            }
        });

        botaoSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/check.png"))); // NOI18N
        botaoSalvar.setText("Cadastrar");
        botaoSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSalvarActionPerformed(evt);
            }
        });

        botaoLimparDados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/clean.png"))); // NOI18N
        botaoLimparDados.setText("Limpar Dados");
        botaoLimparDados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoLimparDadosActionPerformed(evt);
            }
        });

        tabelaClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabelaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaClientesMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tabelaClientesMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaClientes);

        campoDataCadastro.setEditable(false);
        try {
            campoDataCadastro.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        campoDataCadastro.setToolTipText("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 868, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botaoLimparDados)
                        .addGap(549, 549, 549)
                        .addComponent(botaoSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoCancelar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(campoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(105, 105, 105)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(campoDataCadastro))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(campoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(campoDataCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botaoLimparDados)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(botaoSalvar)
                        .addComponent(botaoCancelar)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void campoEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoEmailActionPerformed

    }//GEN-LAST:event_campoEmailActionPerformed

    private void comboSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSexoActionPerformed

    }//GEN-LAST:event_comboSexoActionPerformed

    private void campoTelFixoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoTelFixoActionPerformed

    }//GEN-LAST:event_campoTelFixoActionPerformed

    private void campoTelCelularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoTelCelularActionPerformed

    }//GEN-LAST:event_campoTelCelularActionPerformed

    private void botaoSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSalvarActionPerformed
        this.salvar();
    }//GEN-LAST:event_botaoSalvarActionPerformed

    private void botaoLimparDadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoLimparDadosActionPerformed
        this.novo();
    }//GEN-LAST:event_botaoLimparDadosActionPerformed

    private void botaoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_botaoCancelarActionPerformed

    private void comboEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboEstadoActionPerformed

    }//GEN-LAST:event_comboEstadoActionPerformed

    private void botaoBuscarCEPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoBuscarCEPActionPerformed
        this.buscaCep();
    }//GEN-LAST:event_botaoBuscarCEPActionPerformed

    private void campoLogradouroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoLogradouroActionPerformed
    
    }//GEN-LAST:event_campoLogradouroActionPerformed

    private void campoNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoNomeActionPerformed
     
    }//GEN-LAST:event_campoNomeActionPerformed

    private void menuRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverActionPerformed

        Cliente c = this.getItemSelecionado();

        int dialogResult = JOptionPane.showConfirmDialog(this, "Deseja realmente remover o item selecionado ?",
                "Exclusão",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
            try {
                dao.delete(c);
                if (this.cliente.getId() == c.getId()) {
                    this.novo();
                }

                this.atulizarTabela();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Falha ao remover\nExistem pedidos utilizando este cliente!!!", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_menuRemoverActionPerformed
    private Cliente getItemSelecionado() {
        int row = this.tabelaClientes.getSelectedRow();
        int id = (int) this.tabelaClientes.getModel().getValueAt(row, 0);
        String nome = (String) this.tabelaClientes.getModel().getValueAt(row, 1);
        String CPF = (String) this.tabelaClientes.getModel().getValueAt(row, 2);
        String RG = (String) this.tabelaClientes.getModel().getValueAt(row, 3);
        String telFixo = (String) this.tabelaClientes.getModel().getValueAt(row, 4);
        String telCelular = (String) this.tabelaClientes.getModel().getValueAt(row, 5);
        String CEP = (String) this.tabelaClientes.getModel().getValueAt(row, 6);
        Date dataCadastro = (Date) this.tabelaClientes.getModel().getValueAt(row, 7);

        Cliente c = new Cliente();
        c.setId(id);
        c.setNome(nome);
        c.setCPF(CPF);
        c.setRG(RG);
        c.setTelFixo(telFixo);
        c.setTelCelular(telCelular);
        c.getEndereco().setCEP(CEP);
        c.setDataCadastro(dataCadastro);
        return c;
    }
    private void menuEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEditarActionPerformed
        this.cliente = this.getItemSelecionado();
        this.preencherFormulario();
    }//GEN-LAST:event_menuEditarActionPerformed

    private void tabelaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaClientesMouseClicked
        if (evt.getClickCount() > 1) {
            this.cliente = this.getItemSelecionado();
            this.preencherFormulario();
        }
    }//GEN-LAST:event_tabelaClientesMouseClicked

    private void tabelaClientesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaClientesMouseReleased
        if (evt.getButton() == 3 && this.tabelaClientes.getSelectedRow() != -1) {
            popUpMenu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_tabelaClientesMouseReleased

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new CadastrarCliente().setVisible(true);
        });
    }

    public void centralizarTela() {
        this.setLocationRelativeTo(null);
    }

    private Cliente preencherObjeto() {

        Cliente cliente = new Cliente(this.dataCadastro);

        cliente.setId(Integer.parseInt(this.campoCodigo.getText()));
        cliente.setNome((String) this.campoNome.getText());
        cliente.setCPF((String) this.campoCPF.getText());
        cliente.setRG((String) this.campoRG.getText());
        cliente.setTelFixo((String) this.campoTelFixo.getText());
        cliente.setTelCelular((String) this.campoTelCelular.getText());
        cliente.setSexo((String) this.comboSexo.getSelectedItem());
        cliente.setEmail((String) this.campoEmail.getText());
        cliente.getEndereco().setCEP((String) this.campoCEP.getText());
        cliente.getEndereco().setLogradouro((String) this.campoLogradouro.getText());
        cliente.getEndereco().setNumero(Integer.parseInt(this.campoNumero.getText()));
        cliente.getEndereco().setComplemento((String) this.campoComplemento.getText());
        cliente.getEndereco().setBairro((String) this.campoBairro.getText());
        cliente.getEndereco().setCidade((String) this.campoCidade.getText());
        cliente.getEndereco().setUF((String) this.comboEstado.getSelectedItem());

        return cliente;

    }

    public void atulizarTabela() {

        List<Cliente> lista = dao.findAll();
        String[] col = new String[]{"Cód.", "Nome", "CPF", "RG", "Tel. Fixo", "Tel. Celular", "CEP", "Data Cadastro"};
        DefaultTableModel modelo = new DefaultTableModel(null, col) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Cliente c : lista) {
            Object[] row = new Object[8];
            row[0] = c.getId();
            row[1] = c.getNome();
            row[2] = c.getCPF();
            row[3] = c.getRG();
            row[4] = c.getTelFixo();
            row[5] = c.getTelCelular();
            row[6] = c.getEndereco().getCEP();
            row[7] = c.getDataCadastro();
            modelo.addRow(row);
        }

        this.tabelaClientes.setModel(modelo);
    }

    public void buscaCep() {
        WebServiceCep webService = WebServiceCep.searchCep(campoCEP.getText());

        if (webService.wasSuccessful() && webService.getResulCode() == 1) {
            this.campoLogradouro.setText(webService.getLogradouroFull());
            this.campoBairro.setText(webService.getBairro());
            this.campoCidade.setText(webService.getCidade());
            this.comboEstado.getModel().setSelectedItem(webService.getUf());
            this.campoNumero.requestFocus();
        } else if (webService.getResulCode() == 2) {
            this.campoCEP.setText("");
            this.campoCEP.requestFocus();
            JOptionPane.showMessageDialog(this, "Falha ao buscar CEP", webService.getResultText(), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void salvar() {
        try {

            this.cliente = this.preencherObjeto();

            try {
                if (this.cliente.getId() == 0) {
                    dao.save(this.cliente);
                    this.preencherFormulario();
                    this.atulizarTabela();

                    JOptionPane.showMessageDialog(this, "Salvo com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    dao.update(cliente);
                    this.preencherFormulario();
                    this.atulizarTabela();
                    JOptionPane.showMessageDialog(this, "Atualizado com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Falha ao Salvar!!!", "Erro", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception ex) {

            JOptionPane.showMessageDialog(this, "Favor preencher todos os campos!!!", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void novo() {
        this.cliente = new Cliente();
        this.preencherFormulario();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoBuscarCEP;
    private javax.swing.JButton botaoCancelar;
    private javax.swing.JButton botaoLimparDados;
    private javax.swing.JButton botaoSalvar;
    private javax.swing.JTextField campoBairro;
    private javax.swing.JFormattedTextField campoCEP;
    private javax.swing.JFormattedTextField campoCPF;
    private javax.swing.JTextField campoCidade;
    private javax.swing.JTextField campoCodigo;
    private javax.swing.JTextField campoComplemento;
    private javax.swing.JFormattedTextField campoDataCadastro;
    private javax.swing.JTextField campoEmail;
    private javax.swing.JTextField campoLogradouro;
    private javax.swing.JTextField campoNome;
    private javax.swing.JFormattedTextField campoNumero;
    private javax.swing.JTextField campoRG;
    private javax.swing.JFormattedTextField campoTelCelular;
    private javax.swing.JFormattedTextField campoTelFixo;
    private javax.swing.JComboBox comboEstado;
    private javax.swing.JComboBox comboSexo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem menuEditar;
    private javax.swing.JMenuItem menuRemover;
    private javax.swing.JPopupMenu popUpMenu;
    private javax.swing.JPopupMenu.Separator separador;
    private javax.swing.JTable tabelaClientes;
    // End of variables declaration//GEN-END:variables
}
