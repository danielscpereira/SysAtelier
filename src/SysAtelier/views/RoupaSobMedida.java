package SysAtelier.views;

import SysAtelier.dao.ClienteDAO;
import SysAtelier.dao.EncomendaRoupaDAO;
import SysAtelier.dao.JPAUtil;
import SysAtelier.dao.PedidoSobMedidaDAO;
import SysAtelier.model.Cliente;
import SysAtelier.model.EncomendaRoupa;
import SysAtelier.model.Pedido;
import SysAtelier.model.PedidoSobMedida;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;

public class RoupaSobMedida extends javax.swing.JFrame {

    
    private PedidoSobMedida pedido = new PedidoSobMedida();
    private PedidoSobMedidaDAO dao = new PedidoSobMedidaDAO();
    
    private ClienteDAO clienteDAO = new ClienteDAO();
  

    public RoupaSobMedida() {
        initComponents();
        this.centralizarTela();
        this.preencherFormulario();
       
        EntityManager em = JPAUtil.getEntityManager();
    }

    public RoupaSobMedida(EncomendaRoupa encomendaRoupa) {
        this.pedido.setEmcomenda(encomendaRoupa);
        this.centralizarTela();
        this.preencherFormulario();
    }

    private void preencherCliente() {
        if (this.pedido.getCliente() != null) {
            this.campoNome.setText(this.pedido.getCliente().getNome());
            this.campoCodigoCliente.setText(String.valueOf(this.pedido.getCliente().getId()));
            this.campoCPF.setText(this.pedido.getCliente().getCPF());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboGenero = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        comboTipo = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        campoPunho = new javax.swing.JTextField();
        campoBraco = new javax.swing.JTextField();
        campoTorax = new javax.swing.JTextField();
        campoPescoco = new javax.swing.JTextField();
        campoQuadril = new javax.swing.JTextField();
        campoCintura = new javax.swing.JTextField();
        campoBusto = new javax.swing.JTextField();
        campoComprimentoManga = new javax.swing.JTextField();
        campoAlturaQuadril = new javax.swing.JTextField();
        campoAlturaBusto = new javax.swing.JTextField();
        campoDistanciaBusto = new javax.swing.JTextField();
        campoLarguraCostas = new javax.swing.JTextField();
        campoAlturaCostas = new javax.swing.JTextField();
        campoComprimentoSaia = new javax.swing.JTextField();
        campoComprimentoCalca = new javax.swing.JTextField();
        campoAlturaPernas = new javax.swing.JTextField();
        campoAlturaGancho = new javax.swing.JTextField();
        campoLarguraJoelho = new javax.swing.JTextField();
        campoBocaCalca = new javax.swing.JTextField();
        botaoRealizarPedido = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        campoCodigo = new javax.swing.JTextField();
        botaoCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        campoCodigoCliente = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        campoCPF = new javax.swing.JFormattedTextField();
        jLabel27 = new javax.swing.JLabel();
        campoNome = new javax.swing.JTextField();
        botaoPesquisarCliente = new javax.swing.JButton();
        jLabel28 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados para Medida"));

        jLabel1.setText("Gênero da Fantasia:");

        comboGenero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Feminino" }));
        comboGenero.setSelectedIndex(1);
        comboGenero.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboGeneroItemStateChanged(evt);
            }
        });
        comboGenero.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                comboGeneroFocusLost(evt);
            }
        });
        comboGenero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGeneroActionPerformed(evt);
            }
        });

        jLabel2.setText("Tipo da Fantasia:");

        comboTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Adulto", "Infantil" }));

        jLabel3.setText("Busto:");

        jLabel4.setText("Cintura:");

        jLabel5.setText("Quadril:");

        jLabel6.setText("Pescoço:");

        jLabel7.setText("Tórax:");

        jLabel8.setText("Braço:");

        jLabel9.setText("Punho (mão):");

        jLabel12.setText("Altura das Costas:");

        jLabel13.setText("Largura das Costas:");

        jLabel14.setText("Distância do Busto:");

        jLabel15.setText("Altura do Busto:");

        jLabel16.setText("Comprimento da Manga:");

        jLabel17.setText("Altura do Quadril:");

        jLabel18.setText("Comprimento da Saia:");

        jLabel19.setText("Comprimento da Calça:");

        jLabel20.setText("Altura entre Pernas:");

        jLabel21.setText("Altura do Gancho:");

        jLabel22.setText("Largura do Joelho:");

        jLabel23.setText("Boca da Calça:");

        campoTorax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoToraxActionPerformed(evt);
            }
        });

        campoQuadril.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoQuadrilActionPerformed(evt);
            }
        });

        campoDistanciaBusto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoDistanciaBustoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(comboGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(campoPunho))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3))
                                .addGap(40, 40, 40)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(campoCintura)
                                    .addComponent(campoQuadril)
                                    .addComponent(campoPescoco)
                                    .addComponent(campoTorax)
                                    .addComponent(campoBusto)
                                    .addComponent(campoBraco, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel18)
                                        .addGap(30, 30, 30)
                                        .addComponent(campoComprimentoSaia, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel13)
                                            .addGap(38, 38, 38)
                                            .addComponent(campoLarguraCostas))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel14)
                                            .addGap(43, 43, 43)
                                            .addComponent(campoDistanciaBusto))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel17)
                                            .addGap(50, 50, 50)
                                            .addComponent(campoAlturaQuadril))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel16)
                                                .addComponent(jLabel15))
                                            .addGap(18, 18, 18)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(campoComprimentoManga)
                                                .addComponent(campoAlturaBusto, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel12)
                                            .addGap(46, 46, 46)
                                            .addComponent(campoAlturaCostas, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel20)
                                    .addComponent(jLabel21)
                                    .addComponent(jLabel22)
                                    .addComponent(jLabel23))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(campoLarguraJoelho)
                                    .addComponent(campoAlturaGancho)
                                    .addComponent(campoAlturaPernas)
                                    .addComponent(campoComprimentoCalca, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)
                                    .addComponent(campoBocaCalca, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(16, 16, 16))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(135, 135, 135)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(53, Short.MAX_VALUE))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboGenero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(campoBusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(campoCintura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(campoQuadril, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(campoPescoco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(campoTorax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(campoBraco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(campoAlturaCostas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19)
                            .addComponent(campoComprimentoCalca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(campoLarguraCostas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20)
                            .addComponent(campoAlturaPernas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(campoDistanciaBusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21)
                            .addComponent(campoAlturaGancho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(campoAlturaBusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22)
                            .addComponent(campoLarguraJoelho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(campoComprimentoManga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23)
                            .addComponent(campoBocaCalca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addComponent(campoAlturaQuadril, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(campoPunho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(campoComprimentoSaia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(70, 70, 70))
        );

        botaoRealizarPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/check.png"))); // NOI18N
        botaoRealizarPedido.setText("Realizar Pedido");
        botaoRealizarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoRealizarPedidoActionPerformed(evt);
            }
        });

        jLabel11.setText("Código:");

        campoCodigo.setEditable(false);

        botaoCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/cancel.png"))); // NOI18N
        botaoCancelar.setText("Cancelar");
        botaoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados do Cliente"));

        jLabel10.setText("Código:");

        campoCodigoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoCodigoClienteActionPerformed(evt);
            }
        });

        jLabel26.setText("CPF:");

        try {
            campoCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel27.setText("Nome:");

        botaoPesquisarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/search.png"))); // NOI18N
        botaoPesquisarCliente.setText("Pesquisar");
        botaoPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoPesquisarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addGap(24, 24, 24)
                        .addComponent(campoNome))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(campoCodigoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel26)
                        .addGap(18, 18, 18)
                        .addComponent(campoCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(botaoPesquisarCliente)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(campoCodigoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(campoCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(campoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoPesquisarCliente))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SysAtelier/icons/masc..png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(campoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botaoRealizarPedido)
                        .addGap(18, 18, 18)
                        .addComponent(botaoCancelar))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(campoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoRealizarPedido)
                    .addComponent(botaoCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void campoQuadrilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoQuadrilActionPerformed

    }//GEN-LAST:event_campoQuadrilActionPerformed

    private void campoToraxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoToraxActionPerformed

    }//GEN-LAST:event_campoToraxActionPerformed

    private void campoDistanciaBustoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoDistanciaBustoActionPerformed

    }//GEN-LAST:event_campoDistanciaBustoActionPerformed

    private void botaoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_botaoCancelarActionPerformed

    private void campoCodigoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoCodigoClienteActionPerformed

    }//GEN-LAST:event_campoCodigoClienteActionPerformed

    private void comboGeneroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGeneroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboGeneroActionPerformed

    private void comboGeneroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboGeneroFocusLost

    }//GEN-LAST:event_comboGeneroFocusLost

    private void comboGeneroItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboGeneroItemStateChanged

        if (comboGenero.getSelectedIndex() == 1) {

            campoBusto.setEnabled(true);
            campoDistanciaBusto.setEnabled(true);
            campoAlturaBusto.setEnabled(true);
            campoComprimentoSaia.setEnabled(true);

        } else {
            campoBusto.setEnabled(false);
            campoDistanciaBusto.setEnabled(false);
            campoAlturaBusto.setEnabled(false);
            campoComprimentoSaia.setEnabled(false);
        }


    }//GEN-LAST:event_comboGeneroItemStateChanged

    private void botaoPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoPesquisarClienteActionPerformed
        this.pesquisar();
    }//GEN-LAST:event_botaoPesquisarClienteActionPerformed

    private void botaoRealizarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoRealizarPedidoActionPerformed
       
        
        this.salvar();
        
    }//GEN-LAST:event_botaoRealizarPedidoActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RoupaSobMedida().setVisible(true);
            }
        });
    }

    public void centralizarTela() {
        this.setLocationRelativeTo(null);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoCancelar;
    private javax.swing.JButton botaoPesquisarCliente;
    private javax.swing.JButton botaoRealizarPedido;
    private javax.swing.JTextField campoAlturaBusto;
    private javax.swing.JTextField campoAlturaCostas;
    private javax.swing.JTextField campoAlturaGancho;
    private javax.swing.JTextField campoAlturaPernas;
    private javax.swing.JTextField campoAlturaQuadril;
    private javax.swing.JTextField campoBocaCalca;
    private javax.swing.JTextField campoBraco;
    private javax.swing.JTextField campoBusto;
    private javax.swing.JFormattedTextField campoCPF;
    private javax.swing.JTextField campoCintura;
    private javax.swing.JTextField campoCodigo;
    private javax.swing.JTextField campoCodigoCliente;
    private javax.swing.JTextField campoComprimentoCalca;
    private javax.swing.JTextField campoComprimentoManga;
    private javax.swing.JTextField campoComprimentoSaia;
    private javax.swing.JTextField campoDistanciaBusto;
    private javax.swing.JTextField campoLarguraCostas;
    private javax.swing.JTextField campoLarguraJoelho;
    private javax.swing.JTextField campoNome;
    private javax.swing.JTextField campoPescoco;
    private javax.swing.JTextField campoPunho;
    private javax.swing.JTextField campoQuadril;
    private javax.swing.JTextField campoTorax;
    private javax.swing.JComboBox comboGenero;
    private javax.swing.JComboBox comboTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables

    private void preencherFormulario() {

        this.campoCodigo.setText(String.valueOf(this.pedido.getEmcomenda().getId()));
        this.campoBusto.setText(String.valueOf(this.pedido.getEmcomenda().getBusto()));
        this.campoCintura.setText(String.valueOf(this.pedido.getEmcomenda().getCintura()));
        this.campoQuadril.setText(String.valueOf(this.pedido.getEmcomenda().getQuadril()));
        this.campoPescoco.setText(String.valueOf(this.pedido.getEmcomenda().getPescoco()));
        this.campoTorax.setText(String.valueOf(this.pedido.getEmcomenda().getTorax()));
        this.campoBraco.setText(String.valueOf(this.pedido.getEmcomenda().getBraco()));
        this.campoPunho.setText(String.valueOf(this.pedido.getEmcomenda().getPunho()));
        this.campoAlturaCostas.setText(String.valueOf(this.pedido.getEmcomenda().getAlturaCostas()));
        this.campoLarguraCostas.setText(String.valueOf(this.pedido.getEmcomenda().getLarguraCostas()));
        this.campoDistanciaBusto.setText(String.valueOf(this.pedido.getEmcomenda().getLarguraBusto()));
        this.campoAlturaBusto.setText(String.valueOf(this.pedido.getEmcomenda().getAlturaBusto()));
        this.campoComprimentoManga.setText(String.valueOf(this.pedido.getEmcomenda().getComprimentoManga()));
        this.campoAlturaQuadril.setText(String.valueOf(this.pedido.getEmcomenda().getAlturaQuadril()));
        this.campoComprimentoSaia.setText(String.valueOf(this.pedido.getEmcomenda().getComprimentoSaia()));
        this.campoComprimentoCalca.setText(String.valueOf(this.pedido.getEmcomenda().getComprimentoCalca()));
        this.campoAlturaPernas.setText(String.valueOf(this.pedido.getEmcomenda().getAlturaEntrePernas()));
        this.campoAlturaGancho.setText(String.valueOf(this.pedido.getEmcomenda().getAlturaGancho()));
        this.campoLarguraJoelho.setText(String.valueOf(this.pedido.getEmcomenda().getLarguraJoelho()));
        this.campoBocaCalca.setText(String.valueOf(this.pedido.getEmcomenda().getBocaCalca()));
        this.comboTipo.setSelectedItem(this.pedido.getEmcomenda().getTipoFantasia());
        this.comboGenero.setSelectedItem(this.pedido.getEmcomenda().getGeneroFantasia());
        this.preencherCliente();

    }

    private EncomendaRoupa preencherObjeto() {

       EncomendaRoupa encomendaRoupa = new EncomendaRoupa();

        encomendaRoupa.setId(Integer.parseInt(this.campoCodigo.getText()));
        encomendaRoupa.setBusto(Integer.parseInt(this.campoBusto.getText()));
        encomendaRoupa.setCintura(Integer.parseInt(this.campoCintura.getText()));
        encomendaRoupa.setQuadril(Integer.parseInt(this.campoQuadril.getText()));
        encomendaRoupa.setPescoco(Integer.parseInt(this.campoPescoco.getText()));
        encomendaRoupa.setTorax(Integer.parseInt(this.campoTorax.getText()));
        encomendaRoupa.setBraco(Integer.parseInt(this.campoBraco.getText()));
        encomendaRoupa.setPunho(Integer.parseInt(this.campoPunho.getText()));
        encomendaRoupa.setAlturaCostas(Integer.parseInt(this.campoAlturaCostas.getText()));
        encomendaRoupa.setLarguraCostas(Integer.parseInt(this.campoLarguraCostas.getText()));
        encomendaRoupa.setLarguraBusto(Integer.parseInt(this.campoDistanciaBusto.getText()));
        encomendaRoupa.setComprimentoManga(Integer.parseInt(this.campoComprimentoManga.getText()));
        encomendaRoupa.setAlturaBusto(Integer.parseInt(this.campoAlturaBusto.getText()));
        encomendaRoupa.setAlturaQuadril(Integer.parseInt(this.campoAlturaQuadril.getText()));
        encomendaRoupa.setComprimentoSaia(Integer.parseInt(this.campoComprimentoSaia.getText()));
        encomendaRoupa.setComprimentoCalca(Integer.parseInt(this.campoComprimentoCalca.getText()));
        encomendaRoupa.setAlturaEntrePernas(Integer.parseInt(this.campoAlturaPernas.getText()));
        encomendaRoupa.setAlturaGancho(Integer.parseInt(this.campoAlturaGancho.getText()));
        encomendaRoupa.setLarguraJoelho(Integer.parseInt(this.campoLarguraJoelho.getText()));
        encomendaRoupa.setBocaCalca(Integer.parseInt(this.campoBocaCalca.getText()));
        encomendaRoupa.setGeneroFantasia((String) this.comboGenero.getSelectedItem());
        encomendaRoupa.setTipoFantasia((String) this.comboTipo.getSelectedItem());

        return encomendaRoupa;

    }

    private void salvar() {
        try {

            EncomendaRoupa encomenda = this.preencherObjeto() ; 
            this.pedido.setEmcomenda(encomenda) ;

            if (this.pedido.getEmcomenda().getId() == 0) {
                dao.save(this.pedido);

                JOptionPane.showMessageDialog(this, "Salvo com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
            } else {
                dao.update(this.pedido);
                JOptionPane.showMessageDialog(this, "Atualizado com sucesso", "Informação", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Falha ao Salvar!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        }

        this.preencherFormulario();

    }

    private void pesquisar() {

        int id = 0;

        if (!this.campoCodigoCliente.getText().equals("")) {
            id = (Integer.parseInt(this.campoCodigo.getText()));
        }
        String CPF = "";
        if (!this.campoCPF.getText().replace(".", "").replace("-", "").replace(" ", "").equals("")) {
            CPF = ((String) this.campoCPF.getText());
        }
        String nome = ((String) this.campoNome.getText());

        List<Cliente> lista = clienteDAO.findBYUnit(id, CPF, nome);

        if (lista.size() > 0) {
            this.pedido.setCliente(lista.get(0));
        } else {
            JOptionPane.showMessageDialog(this, "Nao Foram encontados resultados", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        this.preencherCliente();

    }

}
